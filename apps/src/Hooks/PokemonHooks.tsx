import useSWR from 'swr'

const fetcher = (url: RequestInfo | URL) => fetch(url).then((r) => r.json())

export const useGetPokemonList = () => {
  const path = 'https://pokeapi.co/api/v2/pokemon/?limit=1010'

  const { data, error } = useSWR(path, fetcher)

  return {
    data,
    error,
  }
}
