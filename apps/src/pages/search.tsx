import { Box, Card, CardContent, ImageList, ImageListItem, ImageListItemBar, Typography } from '@mui/material'
import { useGetPokemonList } from 'Hooks/PokemonHooks'
import Head from 'next/head'

const Search = () => {
  const { data, error } = useGetPokemonList()

  return (
    <>
      <Head>
        <title>ポケモンサーチ</title>
      </Head>
      <Box display="flex" flexWrap="wrap">
      {data?.results.map((pokemon: { name: string; url: string }) => (
      <Card sx={{ maxWidth: "120px", margin: "4px", padding: "4px" }}>
          <ImageListItem key={pokemon.url} sx={{width: "120px", height: "120px", }}>
            <img
              src={`https://img.pokemondb.net/artwork/${pokemon.name}.jpg`}
              alt={pokemon.name}
              loading="lazy"
              style={{ objectFit: "contain", height: "60px"}}
            />
            <CardContent>
        <Typography gutterBottom variant="h5" sx={{wordBreak: "break-all"}}>
          {pokemon.name}
        </Typography>
        </CardContent>
          </ImageListItem>
      </Card>
        ))}
        </Box>
    </>
  )
}

export default Search
