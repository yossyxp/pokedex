import Link from 'next/link'

const Home = () => {
  return(
    <Link href="/search">ポケモン検索</Link>
  )
}

export default Home
